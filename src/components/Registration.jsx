import React, { useState } from "react";
import "./common.css";
import { FormControl } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import { connect } from "react-redux";
function Registration(props) {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPasword] = useState("");
  function handleFirstName(e) {
    setFirstName(e.target.value);
  }
  function handleLastName(e) {
    setLastName(e.target.value);
  }
  function handleEmail(e) {
    setEmail(e.target.value);
  }
  function handlePassword(e) {
    setPasword(e.target.value);
  }
  function handleRegistration(e) {
    e.preventDefault();
  }
  return (
    <div className="registration-form">
      <Container>
        <form noValidate autoComplete="off" onSubmit={handleRegistration}>
          <FormControl>
            <TextField
              label="First Name"
              variant="outlined"
              value={firstName}
              onChange={handleFirstName}
            />
            <TextField
              label="Last Name"
              variant="outlined"
              value={lastName}
              onChange={handleLastName}
            />
            <TextField
              label="Email"
              variant="outlined"
              value={email}
              onChange={handleEmail}
            />
            <TextField
              label="Password"
              variant="outlined"
              value={password}
              onChange={handlePassword}
            />
            <Button type="submit" variant="contained" color="primary">
              Register Yourself
            </Button>
          </FormControl>
        </form>
      </Container>
    </div>
  );
}

// const mapDispatchToProps = (dispatch) => {
//   return {
//     handleRegistration: (username) => {
//       dispatch({ type: "INSERT_USERNAME", payload: username });
//     },
//   };
// };

export default Registration;
