import React from "react";

const Home = () => {
  return (
    <div className="banner-image">
      <img
        style={{ width: "100%" }}
        className="img-fluid"
        src={`${process.env.PUBLIC_URL}/banner.gif`}
        alt="logo"
      />
    </div>
  );
};
export default Home;
