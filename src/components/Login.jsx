import React, { useState } from "react";
import "./common.css";
import { FormControl } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { connect, useSelector } from "react-redux";
import Welcome from "./Welcome";

// function handlePasswordChange(e) {
//   setPassword(e.target.value);
// }
function Login(props) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [login, setLogin] = useState(false);

  //let storedEmail = useSelector((state) => state.email);

  function handleFormSubmission(e) {
    e.preventDefault();
    if (email === props.email && password == props.userPassword) {
      setLogin(true);
    }
  }

  if (login) {
    return <Welcome />;
  }

  return (
    <div className="registration-form">
      <form noValidate autoComplete="off" onSubmit={handleFormSubmission}>
        <FormControl>
          <TextField
            label="Email"
            variant="outlined"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <TextField
            label="Password"
            variant="outlined"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          <Button type="submit" variant="contained" color="primary">
            Login
          </Button>
        </FormControl>
      </form>
    </div>
  );
}
const mapStateToProps = (state) => {
  return {
    username: state.username,
    email: state.email,
    age: state.age,
    userPassword: state.password,
  };
};
export default connect(mapStateToProps)(Login);
// const mapDispatchToProps = (dispatch) => {
//   return {
//     changeName: (username) => {
//       dispatch({ type: "CHANGE_NAME", payload: username });
//     },
//   };
// };
// export default connect(mapStateToProps, mapDispatchToProps)(Login);
