import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Header from "./components/Header";
import Home from "./components/Home";
import Registration from "./components/Registration";
import About from "./components/About";
import Login from "./components/Login";
import Welcome from "./components/Welcome";
function App() {
  return (
    <Router>
      <div className="App">
        <Header />
        <Switch>
          <Route path="/" exact component={Home}></Route>
          <Route path="/registration" component={Registration} />
          <Route path="/about" component={About}></Route>
          <Route path="/login" component={Login}></Route>
          <Route path="/welcome" component={Welcome}></Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
